import Vue from "vue";
import Vuex from "vuex";
import HomeStore from "./modules/home";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  getters: {},
  modules: {
    Home: HomeStore
  }
});

import axios from "axios";

export default ({
    namespaced: true,
    state: {
        name: "Home",
        data: null,
        images: null,
        loadingData: true

    },
    mutations: {
        SET_DATA(state, data) {
            state.data = data;
        },
        SET_IMAGES(state, images) {
            state.images = images
        },
        TOGGLE_SPINNER(state) {
            if (state.data && state.images) {
                state.loadingData = false
            }
        }
    },
    actions: {
        async fetchHomeData({ commit }) {
            var tmpData = await axios.get("https://jsonplaceholder.typicode.com/posts");
            var tmpImages = await axios.get("https://jsonplaceholder.typicode.com/photos");
            commit("SET_DATA", tmpData.data);
            commit("SET_IMAGES", tmpImages.data);
            commit("TOGGLE_SPINNER");

        }
    },
    getters: {
        name: state => state.name,
        data: state => state.data,
        images: state => state.images,
        loadingData: state => state.loadingData
    }
});